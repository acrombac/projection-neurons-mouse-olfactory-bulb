# Seurat-style analysis

In order to understand better the projection neuron data, we did a standard Seurat-style, or transcriptomics, analysis. We used the Python package `scanpy', but made sure we kept the analysis as similar to Seurat's workflow as possible.

