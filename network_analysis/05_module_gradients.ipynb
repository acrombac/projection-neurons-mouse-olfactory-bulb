{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# 05_module_gradients\n",
    "\n",
    "Jupyter notebook for the study \"Molecular characterization of projection neuron subtypes in the mouse olfactory bulb\". See also our manuscript: <https://www.biorxiv.org/content/10.1101/2020.11.30.405571>\n",
    "\n",
    "June 2021\n",
    "\n",
    "Contact:\n",
    "\n",
    "- Sara Zeppilli, sara_zeppilli@brown.edu\n",
    "- Robin Attey, robin_attey@alumni.brown.edu\n",
    "- Anton Crombach, anton.crombach@inria.fr\n",
    "\n",
    "or any of the other authors on the manuscript."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import os\n",
    "import pickle\n",
    "\n",
    "import numpy as np\n",
    "import matplotlib as mpl\n",
    "import matplotlib.pyplot as plt\n",
    "# To adjust the 'coolwarm' colormap\n",
    "import matplotlib.colors as mcolors\n",
    "\n",
    "import pandas as pd\n",
    "\n",
    "import anndata as ad\n",
    "import scanpy as sc\n",
    "import scanpy.external as sce\n",
    "\n",
    "# Colour management\n",
    "from cycler import cycler"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sc.settings.verbosity = 3\n",
    "sc.logging.print_header()"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Reading in data"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cutoff = 'r0.8_t0.8_mt8'\n",
    "module_linkage = 'ward'"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Looking only at excitatory neurons\n",
    "SEURAT_PATH = \"../../data/2021-04-08_revised_subcluster\"\n",
    "SEURAT_FNAME = 'elife_revisions_subcluster.loom'\n",
    "SEURAT_HIGH_VAR_GENES_FNAME = 'elife_revisions_top_highvar_genes_v3.txt'\n",
    "MM_TF_FNAME = 'mm_mgi_tfs.txt'\n",
    "\n",
    "# PySCENIC related path\n",
    "REGULONS_PATH = \"../../data/2021-05-19_revised_regulons_postprocessed/\"\n",
    "\n",
    "REGULONS_FNAME = \"final_regulons_{}.p\".format(cutoff)\n",
    "AUC_FNAME = \"subcluster_auc_mtx_{}.p\".format(cutoff)\n",
    "MODULES_FNAME = \"regulon_modules_{}_{}.csv\".format(module_linkage, cutoff)\n",
    "\n",
    "CELL_TYPES_FNAME = \"refined_subclustering_seurat_regulons.csv\"\n",
    "# Filtered for PG cells\n",
    "NUCLEI_NOPG_FNAME = \"elife_revisions_nuclei_ids_noPG.txt\"\n",
    "\n",
    "# Output\n",
    "FIG_PATH = \"./figures\""
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def set_olfactory_bulb_figure_params(draw_all_as_vectors=False):\n",
    "    \"\"\"Function to ensure that general settings are set correctly at the start of creating a figure.\"\"\"\n",
    "    # This functions overwrites figure.figsize and sets it to a default of (4, 4) inches.\n",
    "    sc.settings.set_figure_params(vector_friendly=not draw_all_as_vectors, frameon=False)\n",
    "\n",
    "    # Matplotlib style settings\n",
    "    # Figures on screen and written to file\n",
    "    mpl.rcParams['figure.dpi'] = 100\n",
    "    mpl.rcParams['figure.frameon'] = False\n",
    "    mpl.rcParams['figure.autolayout'] = True\n",
    "    mpl.rcParams['figure.constrained_layout.use'] = False\n",
    "    mpl.rcParams['savefig.dpi'] = 300\n",
    "    mpl.rcParams['savefig.transparent'] = False\n",
    "    mpl.rcParams['savefig.format'] = 'pdf'\n",
    "\n",
    "    # Fonts\n",
    "    # Copied from scanpy\n",
    "    mpl.rcParams['font.sans-serif'] = [\n",
    "            'Arial',\n",
    "            'Helvetica',\n",
    "            'DejaVu Sans',\n",
    "            'Bitstream Vera Sans',\n",
    "            'sans-serif',\n",
    "        ]\n",
    "    # font size below includes names of clusters in the UMAP\n",
    "    mpl.rcParams['font.size'] = 6.5\n",
    "    mpl.rcParams['legend.fontsize'] = 7\n",
    "    mpl.rcParams['axes.titlesize'] = 7\n",
    "\n",
    "    # Colour management\n",
    "    # scanpy uses wisdom from https://graphicdesign.stackexchange.com/questions/3682/where-can-i-find-a-large-palette-set-of-contrasting-colors-for-coloring-many-d\n",
    "    mpl.rcParams['axes.prop_cycle'] = cycler(color=sc.plotting.palettes.default_28)\n",
    "    \n",
    " # Auxilary functions to convert from millimeter(s) to inch(es). Matplotlib figure dimensions are in inches.\n",
    "def mm_to_inch(x):\n",
    "    return x * 0.039370\n",
    "\n",
    "def mm_to_inches(x, y):\n",
    "    return (x * 0.039370, y * 0.039370)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Read in regulons, activity matrix, modules"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "with open(os.path.join(REGULONS_PATH, REGULONS_FNAME), 'rb') as infile:\n",
    "    regulons = pickle.load(infile)\n",
    "    \n",
    "name2regulons = {r.name: r for r in regulons}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Read in regulon activity = \"area under the ROC curve\" = the Mann-Whitney U statistic\n",
    "with open(os.path.join(REGULONS_PATH, AUC_FNAME), 'rb') as infile:\n",
    "    auc_mtx = pickle.load(infile)\n",
    "\n",
    "# Hard zeros are missing data\n",
    "auc_mtx[auc_mtx < 1e-5] = np.nan\n",
    "\n",
    "auc_mtx_Z = pd.DataFrame(index=auc_mtx.index)\n",
    "for col in list(auc_mtx.columns):\n",
    "    avg = auc_mtx[col].mean()\n",
    "    auc_mtx_nona = auc_mtx[col].fillna(avg)\n",
    "    auc_mtx_Z[col] = (auc_mtx_nona - avg) / auc_mtx_nona.std(ddof=0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Read in regulon modules\n",
    "regulon_modules = pd.read_csv(os.path.join(REGULONS_PATH, MODULES_FNAME))"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "## Read in expression matrix, remove PG"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Seurat UMAP coordinates and cell type labels\n",
    "adata = ad.read_loom(os.path.join(SEURAT_PATH, SEURAT_FNAME), sparse=False)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "cluster_colors = {\n",
    "    'M1':'#fb8b24',\n",
    "    'M2':'#d90368',\n",
    "    'M3':'#f6bd60',\n",
    "    'ET1':'#56cfe1',\n",
    "    'ET2':'#219ebc',\n",
    "    'ET3':'#846b8a',\n",
    "    'ET4':'#ae25ba',\n",
    "#     'PG':'#e637bf',\n",
    "    'T1':'#3700b3'\n",
    "}"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Get all ids that are not PG, iow we keep all mitral and tufted cells\n",
    "with open(os.path.join(SEURAT_PATH, NUCLEI_NOPG_FNAME)) as infile:\n",
    "    nopg_cell_ids = [line.strip() for line in infile]\n",
    "    \n",
    "# Extract only cell ids and cell type\n",
    "adata = adata[nopg_cell_ids, :]\n",
    "\n",
    "adata.uns['louvain_colors'] = [cluster_colors[key] for key in sorted(cluster_colors.keys())]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Before we continue, check what `adata` looks like\n",
    "print(adata)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Preprocessing, different from the entire subcluster as PG excluded"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Clean-up\n",
    "sc.pp.filter_genes(adata, min_cells=3)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sc.pp.highly_variable_genes(adata, n_top_genes=2000, flavor='seurat_v3')\n",
    "sc.pl.highly_variable_genes(adata)\n",
    "\n",
    "high_var_genes = adata.var.index[adata.var['highly_variable']].values.tolist()"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sc.pp.normalize_per_cell(adata)\n",
    "sc.pp.log1p(adata)\n",
    "# Save this data for plotting later on\n",
    "adata.raw = adata"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Regress out unequal nrs of RNAs captured per nucleus\n",
    "sc.pp.regress_out(adata, ['nCount_RNA'])"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# PAGA graph to extract a curved axis"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sc.pp.scale(adata)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sc.tl.pca(adata, n_comps=30, svd_solver='arpack')\n",
    "sc.pl.pca_variance_ratio(adata, log=True, save=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "pcs_used = 16"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sc.pp.neighbors(adata, n_pcs=pcs_used, n_neighbors=50)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sc.tl.umap(adata, random_state=18)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sc.tl.louvain(adata, resolution=0.35)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sc.pl.umap(adata, color='louvain', legend_fontoutline=2.0, legend_loc='on data')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Let's give clusters names and colors again, redo the above nb cell to see the new colors\n",
    "cluster_names = ['T1', 'M2', 'ET4', 'ET1', 'M1', 'ET2', 'M3', 'ET3']\n",
    "adata.rename_categories('louvain', cluster_names)\n",
    "adata.uns['louvain_colors'] = [cluster_colors[key] for key in cluster_names]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Verify this clustering with the Seurat-based one where PG are present"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "sc.tl.paga(adata)\n",
    "sc.pl.paga(adata, \n",
    "           threshold=0.033, \n",
    "           colors='louvain')"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Visualize for presentations\n",
    "sc.settings.set_figure_params(dpi_save=300, vector_friendly=True)\n",
    "\n",
    "sc.pl.paga_compare(adata,\n",
    "                   title='',\n",
    "                   basis='umap',\n",
    "                   color='louvain', \n",
    "                   legend_fontoutline=2.0, \n",
    "                   save=\".svg\",\n",
    "                   threshold=0.033)\n",
    "#                    pos=1. * (adata.uns['paga']['pos'] - np.min(adata.uns['paga']['pos'], axis=0)))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set a root cell (nucleus) to create trajectories through the data\n",
    "root_cell_index = np.arange(len(adata))[adata.obs['louvain'] == 'ET4'][0]\n",
    "adata.uns['iroot'] = root_cell_index"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Compute diffusion map and pseudotime trajectory. Default parameters are fine for the moment\n",
    "sc.tl.diffmap(adata)\n",
    "sc.tl.dpt(adata)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Visualization: modules ordered along PAGA pseudotime"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def truncate_colormap(cmap, minval=0.0, maxval=1.0, n=200):\n",
    "    new_cmap = mcolors.LinearSegmentedColormap.from_list(\n",
    "        'trunc({n},{a:.2f},{b:.2f})'.format(n=cmap.name, a=minval, b=maxval),\n",
    "        cmap(np.linspace(minval, maxval, n)))\n",
    "    return new_cmap\n",
    "\n",
    "# Here are the two color maps that we use, one for zscored expression data, one for raw expression data\n",
    "zscore_cmap = plt.get_cmap('coolwarm')\n",
    "raw_cmap = truncate_colormap(zscore_cmap, 0.4, 1.0)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Ignore ET3\n",
    "my_path = ['ET4', 'ET1', 'T1', 'M2', 'M3', 'M1', 'ET2']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Make a heatmap with nuclei ordered by pseudotime as columns through paga_path, then\n",
    "# regulons ordered by modules\n",
    "regulon_modules_dict = regulon_modules.set_index('regulon').groupby('module').groups\n",
    "\n",
    "mas_mtx_Z = []\n",
    "for module_name, regulon_names in regulon_modules_dict.items():\n",
    "    # Get the activity levels\n",
    "    mas_mtx_Z.append(auc_mtx_Z[[r for r in regulon_names]].median(axis=1))\n",
    "\n",
    "mas_mtx_Z = pd.DataFrame(np.column_stack(mas_mtx_Z), \n",
    "                       index=adata.obs_names, \n",
    "                       columns=regulon_modules.groupby('module').groups.keys())\n",
    "\n",
    "for module_name in regulon_modules_dict.keys():\n",
    "    adata.obs[module_name] = mas_mtx_Z[module_name]"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Not ideal, as we cannot control vmin and vmax, but at least we can get the data!\n",
    "win = 100\n",
    "_, ordered_data = sc.pl.paga_path(adata, nodes=my_path, \n",
    "                    keys=regulon_modules_dict.keys(),\n",
    "                    n_avg=win,\n",
    "                    color_map=zscore_cmap,\n",
    "                    title='',\n",
    "                    normalize_to_zero_one=False,\n",
    "                    show_colorbar=True,\n",
    "                    show_yticks=True,\n",
    "                    return_data=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Create our own heatmap\n",
    "mod_names = ['mod{}'.format(x) for x in range(1, 8)]\n",
    "vmin, vmax = -.75, .75\n",
    "\n",
    "im = plt.imshow(ordered_data[mod_names].values.T, \n",
    "                aspect='auto',\n",
    "                cmap=zscore_cmap,\n",
    "                norm=None,\n",
    "                vmin=vmin,\n",
    "                vmax=vmax,\n",
    "                interpolation='nearest'\n",
    "               )\n",
    "\n",
    "# Beautify it\n",
    "ax = im.axes\n",
    "for loc in ['top', 'bottom', 'right', 'left']:\n",
    "    ax.spines[loc].set_visible(False)\n",
    "\n",
    "# Tick labels only on y axis\n",
    "ax.set_xticks([])\n",
    "ax.set_yticks([0, 1, 2, 3, 4, 5, 6])\n",
    "ax.set_yticklabels(mod_names)\n",
    "ax.tick_params(length=0)\n",
    "\n",
    "# Create colorbar\n",
    "cbar = ax.figure.colorbar(im, ax=ax, ticks=[vmin, 0, vmax])\n",
    "\n",
    "# And save it\n",
    "# ax.get_figure().savefig(os.path.join('figures', 'paga_selfmade_heatmap.svg'), dpi=600)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Visualization: regulons along pseudotime\n",
    "\n",
    "Make a heatmap with nuclei ordered by pseudotime as columns through paga_path, then\n",
    "regulons clustered within each module."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Ignore ET3\n",
    "my_path = ['ET4', 'ET1', 'T1', 'M2', 'M3', 'M1', 'ET2']"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Make a heatmap with nuclei ordered by pseudotime as columns through paga_path, then\n",
    "# regulons ordered by modules\n",
    "regulon_modules_dict = regulon_modules.set_index('regulon').groupby('module').groups\n",
    "regulon_names = [r for rnames in regulon_modules_dict.values() for r in rnames]\n",
    "\n",
    "mas_mtx_Z = []\n",
    "for module_name, r_names in regulon_modules_dict.items():\n",
    "    # Get the activity levels\n",
    "    for r in r_names:\n",
    "        mas_mtx_Z.append(auc_mtx_Z[r])\n",
    "\n",
    "mas_mtx_Z = pd.DataFrame(np.column_stack(mas_mtx_Z), \n",
    "                       index=adata.obs_names, \n",
    "                       columns=regulon_names)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Make a heatmap with nuclei ordered by pseudotime as columns through paga_path, then\n",
    "# regulons ordered by modules\n",
    "import scipy.spatial.distance as sci_distance\n",
    "import scipy.cluster.hierarchy as sci_hierarchy"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "for rname in mas_mtx_Z.columns.values:\n",
    "    adata.obs[rname] = mas_mtx_Z[rname]\n",
    "\n",
    "# Cluster regulons within modules\n",
    "win = 100\n",
    "_, ordered_data = sc.pl.paga_path(adata, nodes=my_path,\n",
    "                keys=mas_mtx_Z.columns.values,\n",
    "                n_avg=win,\n",
    "                title='',\n",
    "                normalize_to_zero_one=False,\n",
    "                return_data=True)\n",
    "\n",
    "clustered_names_mod = []\n",
    "for module_name, r_names in regulon_modules_dict.items():\n",
    "    idx_mod = sci_hierarchy.leaves_list(\n",
    "                sci_hierarchy.linkage(ordered_data.loc[:, r_names].T, \n",
    "                                  method='average',\n",
    "                                  metric='cosine'))\n",
    "    clustered_names_mod.extend([r_names[i] for i in idx_mod])\n",
    "    \n",
    "# Now we simply need to index ordered_data by clustered_names_mod"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(nrows=1, ncols=1, figsize=mm_to_inches(80, 180))\n",
    "\n",
    "vmin, vmax = -1., 1.\n",
    "im = ax.imshow(ordered_data[clustered_names_mod].values.T, \n",
    "                aspect='auto',\n",
    "                cmap=zscore_cmap,\n",
    "                norm=None,\n",
    "                vmin=vmin,\n",
    "                vmax=vmax,\n",
    "                interpolation='nearest'\n",
    "               )\n",
    "\n",
    "# Beautify it\n",
    "ax = im.axes\n",
    "ax.grid(False)\n",
    "for loc in ['top', 'bottom', 'right', 'left']:\n",
    "    ax.spines[loc].set_visible(False)\n",
    "\n",
    "# Tick labels only on y axis\n",
    "ax.set_xticks([])\n",
    "ax.set_yticks(range(len(clustered_names_mod)))\n",
    "ax.set_yticklabels(clustered_names_mod, fontsize=5.5)\n",
    "ax.tick_params(length=0)\n",
    "\n",
    "# Create colorbar\n",
    "cbar = ax.figure.colorbar(im, ax=ax, ticks=[vmin, 0, vmax])\n",
    "\n",
    "# And save it\n",
    "ax.get_figure().savefig(os.path.join('figures', 'paga_selfmade_regulon_heatmap.svg'), dpi=600)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Visualization: And do the same for TFs"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "win = 100\n",
    "_, ordered_tf_data = sc.pl.paga_path(adata, \n",
    "                                     nodes=my_path, \n",
    "                                     keys=[name[:-3] for name in clustered_names_mod],\n",
    "                                     use_raw=True,\n",
    "                                     n_avg=win,\n",
    "                                     title='',\n",
    "                                     normalize_to_zero_one=True,\n",
    "                                     return_data=True)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "fig, ax = plt.subplots(nrows=1, ncols=1, figsize=mm_to_inches(80, 180))\n",
    "\n",
    "vmin, vmax = 0, 1.\n",
    "im = ax.imshow(ordered_tf_data[[name[:-3] for name in clustered_names_mod]].values.T, \n",
    "                aspect='auto',\n",
    "                cmap=raw_cmap,\n",
    "                norm=None,\n",
    "                vmin=vmin,\n",
    "                vmax=vmax,\n",
    "                interpolation='nearest'\n",
    "               )\n",
    "\n",
    "# Beautify it\n",
    "ax = im.axes\n",
    "ax.grid(False)\n",
    "for loc in ['top', 'bottom', 'right', 'left']:\n",
    "    ax.spines[loc].set_visible(False)\n",
    "\n",
    "# Tick labels only on y axis\n",
    "ax.set_xticks([])\n",
    "ax.set_yticks(range(len(clustered_names_mod)))\n",
    "ax.set_yticklabels([name[:-3] for name in clustered_names_mod], fontsize=5.5)\n",
    "ax.tick_params(length=0)\n",
    "\n",
    "# Create colorbar\n",
    "cbar = ax.figure.colorbar(im, ax=ax, ticks=[vmin, 0, vmax])\n",
    "\n",
    "# And save it\n",
    "ax.get_figure().savefig(os.path.join('figures', 'paga_selfmade_tf_heatmap.svg'), dpi=600)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {},
   "source": [
    "# Visualization: Map module activity onto UMAP"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Set our defaults\n",
    "set_olfactory_bulb_figure_params()\n",
    "\n",
    "fig, axs = plt.subplots(nrows=2, ncols=4, figsize=mm_to_inches(140, 90))\n",
    "\n",
    "for mname, ax in zip(['mod1', 'mod2', 'mod3', 'mod4', 'mod5', 'mod6', 'mod7', 'louvain'], axs.flat):\n",
    "    if mname == 'louvain':\n",
    "        sc.pl.umap(adata, \n",
    "                   color=mname,\n",
    "                   size=3,\n",
    "                   legend_loc='on data',\n",
    "                   legend_fontoutline=2.0,\n",
    "                   show=False, \n",
    "                   ax=ax)        \n",
    "    else:\n",
    "        sc.pl.umap(adata, \n",
    "                   color=mname,\n",
    "                   size=3,\n",
    "                   cmap='coolwarm', \n",
    "                   vmin=-1.5, vmax=1.5, \n",
    "                   show=False, \n",
    "                   ax=ax)\n",
    "\n",
    "    ax.set_aspect('equal')\n",
    "    ax.set_xlabel(None)\n",
    "    ax.set_ylabel(None);\n",
    "\n",
    "# axs.flat[-1].set_visible(False)\n",
    "axs.flat[-1].set_title('cell types')\n",
    "    \n",
    "# Remove colorbars from all graphs as we will make our own\n",
    "# Colorbars are \"hidden\" axes that we need to retrieve from the figure\n",
    "for cb in fig.get_axes():\n",
    "    if cb not in axs:\n",
    "        fig.delaxes(cb)\n",
    "        \n",
    "fig.tight_layout()\n",
    "fig.savefig(os.path.join(FIG_PATH, 'modules_on_umap.svg'), pad_inches=mm_to_inch(1))\n",
    "# fig.savefig(os.path.join(FIG_PATH, 'modules_on_umap.png'), pad_inches=0.01, dpi=300)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": []
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.6.8"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
