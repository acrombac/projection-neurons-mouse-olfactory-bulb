# Gene regulatory network analysis

After we have computed 100 sets of regulons using PySCENIC, we analyze them. We compute modules of regulons on the basis of the connection specificity index; we subdivide the transcriptomically-defined cell types on the basis of their regulon activity; and we quantify gradients of activity in modules and regulons, plus their correspondance to transcription factor expression levels. In addition, we show if and where marker genes are found in the regulons.

Below a quick description of the Jupyter notebooks:

1. **filter_regulons_compute_auc_matrix**: given regulons and their target genes selected at a given confidence cut-off (default 0.8), we keep regulons with >7 target genes. We then proceed by calculating the AUC matrix, also known as the regulon activity matrix. Finally, we check at various confidence cut-offs the presence of marker genes derived from the transcriptome analysis.

2. **regulon_modules**: regulons are clustered by connection specificity index (CSI), a metric for detecting modules in networks ([Green et al.](https://doi.org/10.1016/j.cell.2011.03.037)). Next, we characterize the resulting regulon modules.

3. **refine_seurat_clustering**: the transcriptome analysis (i.e. Seurat-style analysis) resulted in several mitral and tufted cell types. Here we visualize these data in "GRN space" by clustering nuclei within each of the cell types according to their regulon activity. We observe that cell types may be subdivided into subtypes and we do so.

4. **regulon_module_clustering**: we condense the data set from *regulon activity per cell* to *module activity per cell subtype*. We verify by inspecting histograms that all entries in the condensed matrix have a unimodal distribution and thus can be summarized using the mean.

5. **module_gradients**: using PAGA ([Partition-based graph abstraction](https://genomebiology.biomedcentral.com/articles/10.1186/s13059-019-1663-x)) we quantify module activity, regulon activity, and TF expression levels along a trajectory of cell types.

6. **clustering_by_regulon_activity_score**: compare the transcriptome clustering results to clustering on the basis of the regulon activity matrix.

7. **regulon_network**: build transcription factor regulatory networks with a variety of attributes (TF outdegree, cycle membership, top 10 cell type specific regulon activity, marker genes at given confidence cut-off). We use [Cytoscape](https://cytoscape.org/) to visualize the networks.
